package org.test;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;

/**
 *
 */
@Theme("mytheme")
@Widgetset("org.test.MyAppWidgetset")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();
        
        final TextField fname = new TextField();
        fname.setCaption("First name:");

        // Create a text field
        final TextField lname = new TextField();
        lname.setCaption("Last name");

        final TextField grade = new TextField();
        grade.setCaption("Grade");
        // Put some initial content in it
//        lname.setValue("finally");
        // Handle changes in the value
//        lname.addValueChangeListener(event ->
                // Do something with the value
//                Notification.show("Value is: " + lname.getValue()));


        Button button = new Button("Click Me");
        button.addClickListener( e -> {
            layout.addComponent(new Label("Hi " + fname.getValue() + " " + lname.getValue()
                    + ", you got " + grade.getValue() + "!" ));
        });
        
        layout.addComponents(fname, lname, grade, button);
        layout.setMargin(true);
        layout.setSpacing(true);
        
        setContent(layout);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
